import { useState, useEffect } from 'react'
import Loading from './Loading'
import Tours from './Tours'

const url = 'https://course-api.com/react-tours-project'

const App = () => {
  const [loadingScreen, setLoadingScreen] = useState(true)
  const [toursScreen, setToursScreen] = useState(null)
  const removeTour = (id) => {
    const newItem = toursScreen.filter((setItem) => setItem.id !== id)
    setToursScreen(newItem)
  }

  const fetchTours = async () => {
    try {
      const response = await fetch(url)
      const tours = await response.json()
      setToursScreen(tours)
    } catch (error) {
      console.log('error')
    }
    setLoadingScreen(false)
  }
  useEffect(() => {
    fetchTours()
  }, [])
  if (loadingScreen) {
    return <Loading />
  }
  if (toursScreen.length === 0) {
    return (
      <main>
        <div className="title">
          <h2>no tours left</h2>
          <button className="btn" onClick={() => fetchTours()}>
            refresh
          </button>
        </div>
      </main>
    )
  }
  return (
    <section>
      <Tours toursScreen={toursScreen} removeTour={removeTour} />
    </section>
  )
}
export default App
